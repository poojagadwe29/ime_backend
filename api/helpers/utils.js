'use strict'

var request = require('request');


function getUrl(req, url) {
  var options = {
    url: url,
    headers: {
      'Authorization': req.headers.authorization,
      'Content-Type': 'application/json',
      // 'partyContact':req.headers.partyContact
    }
  };
  return options;
}

function modifyUrl(req) {
  var urlParts = req.originalUrl.split("?");
  var param = new URLSearchParams(urlParts[1]);
  param.delete('page');
  param.delete('count');
  var obj = {
    page: req.query.page,
    count: req.query.count,
    param: param
  }
  return obj;
}

function getErrorObj(statusCode, message) {
  var resp = {
    "statusCode": statusCode,
    "message": message
  }
  return resp;
}

function getDeal(req, dealUrl) {
  return new Promise((resolve, reject) => {
    request(this.getUrl(req, dealUrl), function (error, response, body) {
      if (error) {
        reject(error);
      }
      else {
        if (response.statusCode == 200) {
          var resp = JSON.parse(body);
          var data = resp['data'];
          resolve(data);
        }
        else {
          reject(getErrorObj(response.statusCode, response.statusMessage));
          // reject(this.getErrorObj(400,'ddcfc'));
        }
      }

    })
  })
}

function toQueryParams(obj) {
  let queryParamString = '';
  if (!obj) {
    return '';
  }
  Object.keys(obj).forEach((key, i) => {
    if (queryParamString === '') {
      queryParamString = '?';
    }
    queryParamString += ((i !== 0) ? '&' : '')
      + `${key}=${encodeURIComponent((typeof (obj[key]) === 'object' ? JSON.stringify(obj[key]) : obj[key]))}`;
  });
  return queryParamString;
}


module.exports = {
  getUrl: getUrl,
  modifyUrl: modifyUrl,
  getErrorObj: getErrorObj,
  getDeal: getDeal,
  toQueryParams : toQueryParams
}
