// var baseUrl = "http://13.234.119.0:32081/";
var baseUrl = "http://13.234.119.0:32080/";

e = {
    businessSegments: baseUrl.concat('mdm/api/v1/businessSegments'),
    templates: baseUrl.concat('mdm/api/v1/templates'),
    processingUnits: baseUrl.concat('mdm/api/v1/processingUnits'),
    dealParties: baseUrl.concat('deals/live/api/v1/dealParties'),
    liveDeals: baseUrl.concat('deals/live/api/v1/deals'),
    partyResponsibility: baseUrl.concat('mdm/api/v1/partyResponsibilities'),
    parties: baseUrl.concat('mdm/api/v1/parties'),
    dealPartyContacts: baseUrl.concat('deals/live/api/v1/partyContacts'),
    mdmPartyContacts: baseUrl.concat('mdm/api/v1/partyContacts'),
    contacts: baseUrl.concat('bff/api/v1/contacts'),
    contactTypes: baseUrl.concat('mdm/api/v1/contactTypes'),
    accounts: baseUrl.concat('cbp/api/v1/accounts'),
    Transactions: baseUrl.concat('execution/api/v1/scheduler'),
    budgetGroup : baseUrl.concat('deals/live/api/v1/budgetGroups'),
    dealPartyDocuments : baseUrl.concat('deals/live/api/v1/dealPartyDocuments'),
    dealDocuments : baseUrl.concat('deals/live/api/v1/dealDocuments'),
    documentType: baseUrl.concat('mdm/api/v1/documentTypes'),
    partyDocuments : baseUrl.concat('mdm/api/v1/partyDocuments'),
    budgets : baseUrl.concat('deals/live/api/v1/budgets/list'),
    budgetDetail: baseUrl.concat('deals/live/api/v1/budgets'),
    budgetAuditReport: baseUrl.concat('deals/live/api/v1/budgetAuditReport'),
    documentScheduler: baseUrl.concat('execution/api/v1/documentScheduler/list')
}

module.exports = e;