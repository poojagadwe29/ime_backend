'use strict';

var request = require('request');
const async = require("async");
const config = require("../helpers/config");
const utils = require('../helpers/utils');
var cloneDeep = require('lodash.clonedeep');
var accountController = require('../controllers/accountController');
var transactionController = require('../controllers/transactionController');

module.exports = {
    getBudgets: getBudgets,
    getBudgetTypeDetail: getBudgetTypeDetail,
    getTransaction: getTransaction
}

function getBudgets(req, res) {
    var partyContactName = 'anmol';
    var filter = [{
        "searchBy": "partyContact",
        "searchQuery": partyContactName
    }]
    var dealUrl = `${config.liveDeals}?&filter=${JSON.stringify(filter)}&count=-1`
    console.log('Deal Url :', dealUrl);
    utils.getDeal(req, dealUrl).then(async function (deals) {
        var finalResult = [];
        if (req.query.page && req.query.count) {
            let budgetGroups = await getBudgetGroups(req, deals.map(d => d.refId), req.query.page, req.query.count);
            async.eachSeries(deals, function (deal, dealCallback) {
                let result = {
                    dealId: deal.refId,
                    dealName: deal.name,
                    deal: deal._id
                }
                var bdgGrps = budgetGroups.filter(bg => bg.refId === deal.refId);
                async.eachSeries(bdgGrps, function (bdgGrp, innerCallback) {
                    var updatedResult = cloneDeep(result);
                    async.waterfall([
                        async.apply(accountDetailsCall, updatedResult),
                        budgetCall,
                        pushData
                    ], function (err, result) {
                        innerCallback();
                    })

                    function accountDetailsCall(updatedResult, cb) {
                        let accObj = {
                            accountNumber: bdgGrp.accountNumber
                        }
                        accountController.getAccountDetails(req, accObj).then(function (accountDetaiils) {
                            console.log('accountDetaiils :', accountDetaiils);
                            updatedResult.accountBalance = accountDetaiils.accountBalance;
                            updatedResult.currency = accountDetaiils.currency;
                            cb(null, updatedResult);
                        }).catch(err => {
                            console.log('err :', err);
                            cb(err, null);
                        })
                    }

                    function budgetCall(updatedResult, cb) {
                        var budgetUrl = config.budgets.concat('?filter={"budgetGroupId":' + JSON.stringify(bdgGrp._id) + '}&count=-1')
                        getBudget(req, budgetUrl).then(function (budgets) {
                            var bdgArr = [];
                            budgets.forEach(budget => {
                                var bdg = getBudgetName(budget);
                                bdg.id = budget._id
                                bdgArr.push(bdg)
                            })
                            updatedResult.budgetName = bdgArr
                            cb(null, updatedResult)
                        }).catch(err => {
                            console.log('err :', err);
                            cb(err, null);
                        })
                    }

                    function pushData(updatedResult, cb) {
                        updatedResult.accountNumber = bdgGrp.accountNumber
                        updatedResult.fiscalStart = bdgGrp.fiscalStart
                        updatedResult.budgetGroupName = bdgGrp.name
                        updatedResult.budgetGroupId = bdgGrp._id
                        finalResult.push(updatedResult);
                        cb(null, finalResult);
                    }

                    // }
                }, function () {
                    dealCallback();
                })
            }, function () {
                res.json(finalResult);
            })
        }
    }).catch(err => {
        console.log('err :', err);
    })
}

function getBudgetName(budget) {
    var budgetName = {}
    console.log('budget.purpose :', budget.purpose);
    console.log('budget.destination:', budget.destination);
    if (budget.purpose && budget.destination) {
        budgetName = {
            purpose: budget.purpose,
            destination: budget.destination,
            name: budget.purpose.concat(" " + budget.destination)
        }
    }
    else if (budget.purpose) {
        budgetName = {
            purpose: budget.purpose,
            destination: budget.destination,
            name: budget.purpose
        }
    }
    else if (budget.destination) {
        budgetName = {
            purpose: budget.purpos,
            destination: budget.destination,
            name: budget.destination
        }
    }
    else {
        budgetName = {
            purpose: budget.purpos,
            destination: budget.destination,
            name: 'Consolidated'
        }
    }
    return budgetName;
}



function getBudgetGroups(req, budgetGroupIds, page, count) {
    console.log('******************************************* 2nd API Hit *******************************************')
    console.log('**** BudgetGroupIds :', budgetGroupIds);
    var budgetGroupsUrl = config.budgetGroup.concat('?filter={"refId":{"$in":' + JSON.stringify(budgetGroupIds) + '}}');
    if (page && count) {
        budgetGroupsUrl = budgetGroupsUrl.concat('&page=' + page).concat('&count=' + count);
    }
    else {
        budgetGroupsUrl = budgetGroupsUrl.concat('&count=-1');
    }
    console.log('Budget Group Url :', budgetGroupsUrl);
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, budgetGroupsUrl), (err, resp, body) => {
            if (err) {
                reject(err);
            }
            else {
                if (resp.statusCode == 200) {
                    var budgetGroup = JSON.parse(body);
                    resolve(budgetGroup);
                }
                else {
                    reject(utils.getErrorObj(resp.statusCode, res.message));
                }
            }


        })
    })
}

function getBudget(req, budgetUrl) {
    console.log('budget Url :', budgetUrl);
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, budgetUrl), (err, resp, body) => {
            if (err) {
                reject(err);
            }
            else {
                if (resp.statusCode == 200) {
                    var budget = JSON.parse(body);
                    resolve(budget);
                }
                else {
                    reject(utils.getErrorObj(resp.statusCode, res.message));
                }
            }
        })
    })
}

function getBudgetTypeDetail(req, res) {
    var budget = {};
    // var budgetId = req.query.budgetId;
    var purpose = req.query.purpose ? req.query.purpose : "";
    delete req.query.purpose;
    var destination = req.query.destination ? req.query.destination : "";
    delete req.query.destination;
    var fiscalStart = req.query.fiscalStart;
    delete req.query.fiscalStart;
    var budgetUrl = `${config.budgetDetail}${utils.toQueryParams(req.query)}&fiscalStart[year]=${fiscalStart.year}&fiscalStart[month]=${fiscalStart.month}&count=-1`;
    console.log('budgetUrl :', budgetUrl);
    getBudgetType(req, budgetUrl).then(function (budgets) {
        var budget = budgets.filter(budget => budget.purpose === purpose && budget.destination === destination)[0];
        budget['values'].forEach((val, index) => {
            var id = val._id;
            if (id) {
                var budgetUrl = config.budgets.concat('?filter={"_id":' + JSON.stringify(id) + '}&count=-1')
                getBudget(req, budgetUrl).then(function (result) {
                    val.wholeDuration = result[0].duration
                    if (budget['values'].length - 1 === index) {
                        res.json(budget)
                    }
                })
            }
        })
    }).catch(err => {
        console.log('err :', err);
    })
}

function getBudgetType(req, budgetUrl) {
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, budgetUrl), (err, resp, body) => {
            if (err) {
                reject(err);
            }
            else {
                if (resp.statusCode == 200) {
                    var budget = JSON.parse(body);
                    resolve(budget);
                }
                else {
                    reject(utils.getErrorObj(resp.statusCode, res.message));
                }
            }
        })
    })
}



function getTransaction(req, res) {
    var finalResult = [];
    var filter = req.query.filter;
    var url = `${config.budgetAuditReport}?filter=${JSON.parse(JSON.stringify(filter))}&count=-1`;
    console.log('url :', url);
    getBudgetAuditReport(req, url).then(function (results) {
        results.forEach((element, index) => {
            let result = {
                drAccount: element.sourceAccountNumber,
                destinationAccountNumber: element.destinationAccountNumber,
                beneficiaryName: element.beneficiaryName,
                bicCode: element.ifscCode,
                purpose: element.subInstructionPurpose,
                txnAmt: element.amountTobeSettled,
                status: transactionController.statusEnum[element.status],
                settledAmount: element.settledAmount,
                utilisedAmount: element.budgetUtilisedForBudgetId.utilisedAmount,
                allocatedBudgetAmount: element.budgetUtilisedForBudgetId.allocatedBudgetAmount,
                currency: element.currency,
                executionStartAt: element.executionStartAt
            }

            finalResult.push(result);
            if (results.length - 1 === index) {
                res.json(finalResult);
            }
        })
    }).catch(err => {
        console.log('err :', err);
    })
}

function getBudgetAuditReport(req, url) {
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, url), (err, resp, body) => {
            if (err) {
                reject(err);
            }
            else {
                if (resp.statusCode == 200) {
                    var budget = JSON.parse(body);
                    resolve(budget.data);
                }
                else {
                    reject(utils.getErrorObj(resp.statusCode, res.message));
                }
            }
        })
    })

}

