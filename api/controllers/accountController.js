'use strict';

var request = require('request');
const async = require("async");
const config = require("../helpers/config");
const utils = require('../helpers/utils');
var dealController = require('./dealController');

const express = require('express');
const router = express.Router();


// router.get("/getAccounts", (req, res) => {
//TODO: Assuming, from Token -> I have to extract userId and from that userId need to fetch data.
// Hit partyContact to get partContactName as login user is mapped there and pass this partyContact filter to live deals
function getAccounts(req, res) {
    var accounts = [];
    var partyContactName = 'anmol';
    var filter = [{
        "searchBy": "partyContact",
        "searchQuery": partyContactName
    }]
    var url = config.liveDeals.concat('?');
    url = url.concat('&filter=').concat(JSON.stringify(filter));
    console.log('url :', url);
    request(utils.getUrl(req, url), function (err, response, body) {
        if (!err && response.statusCode == 200) {
            body = JSON.parse(body);
            if ("data" in body) {
                var data = body['data'];
                console.log('data.length :', data.length);
                var accountCount = 0;
                if (data != null && data.length > 0) {
                    async.eachSeries(data, function (item, outerCallback) {
                        async.eachSeries(item.accounts, async function (element, innerCallback) {
                            let accObj = {
                                accountNumber: element.accountNumber,
                                accountName: element.name,
                                dealName: item.name,
                                dealId: item.refId,
                            }
                            await getAccountDetails(req, accObj).then(function (res) {
                                accounts.push(res);
                                innerCallback;
                            }).catch(err => {
                                console.log('err :', err);
                            });

                        }, function () {
                            outerCallback();
                        })
                    }, function () {
                        var searchObj = req.query;
                        if (searchObj.filter && searchObj.filter.length > 0) {
                            searchObj = JSON.parse(searchObj.filter)
                            accounts = accounts.filter(acc => acc.accountName.toLowerCase().includes(searchObj[0].searchQuery.toLowerCase()))
                        }
                        res.json(accounts);
                    })
                }
                else {
                    res.json({ message: 'No data found' });
                }
            }
        }
        else {
            if (err) {
                res.status(err.code).json({ 'message': err.message, 'statusCode': err.code });
            }
            else {
                res.status(response.statusCode).json({ 'message': response.statusMessage, 'statusCode': response.statusCode });
            }
        }
    })
}



function getAccountDetails(req, accObj) {
    var accountsUrl = config.accounts.concat('/').concat(accObj.accountNumber);
    console.log('accountsUrl :', accountsUrl);
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, accountsUrl), (err, resp, body) => {
            if (err) {
                reject(utils.getErrorObj(err.statusCode, err.message));
            }
            else {
                if (resp.statusCode == 200) {
                    var data = JSON.parse(body);
                    // TODO: Dont know if there is an array then how to calculate balance.
                    accObj.accountStatus = data.status,
                        accObj.accountBalance = data.balances.available,
                        accObj.accountOpeningDate = data.openedOn,
                        accObj.currency = data.currency
                    console.log('obj :', accObj);
                    resolve(accObj);
                }
                else {
                    reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
                }
            }
        })
    })

}


module.exports = {
    getAccounts: getAccounts,
    getAccountDetails: getAccountDetails
}