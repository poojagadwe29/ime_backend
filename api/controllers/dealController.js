'use strict';

module.exports = {
  getDeals: getDeals,
  getParties: getParties,
  getContactDetails: getContactDetails,
  getDoucments: getDoucments
  // getDealDocuments: getDealDocuments,
  // getDealReqDocuments: getDealReqDocuments
}

var request = require('request');
const async = require("async");
const config = require("../helpers/config");
const utils = require('../helpers/utils');
var cloneDeep = require('lodash.clonedeep');

function businessSegmentsCall(req) {
  return new Promise((resolve, reject) => {
    request(getOptionObj(req, config.businessSegments.concat('?select=_id,name&count=-1')), (businessError, businessResponse, businessBody) => {
      if (businessError) {
        reject(err);
      }
      else {
        var resp = JSON.parse(businessBody);
        resolve(resp);
      }
    })
  })
}

function getProducts(req) {
  return new Promise((resolve, reject) => {
    request(getOptionObj(req, config.templates.concat('?select=_id,name&count=-1')), (productError, productResponse, productBody) => {
      if (productError) {
        reject(err);
      }
      else {
        var resp = JSON.parse(productBody);
        // console.log('resp :', resp);
        resolve(resp);
      }
    })
  })
}


function getProcessingUnits(req) {
  return new Promise((resolve, reject) => {
    request(getOptionObj(req, config.processingUnits.concat('?select=name,_id&count=-1')), (processingError, processingResponse, processingBody) => {
      if (processingError) {
        reject(err);
      }
      else {
        var resp = JSON.parse(processingBody);
        // console.log('resp :', processingBody);
        resolve(processingBody);
      }
    })
  })
}


function getDealParties(req, ids) {
  return new Promise((resolve, reject) => {
    var url = config.dealParties + '?filter={"_id":{"$in":' + JSON.stringify(ids) + '}}&count=-1';
    // console.log('url :', url);
    request(getOptionObj(req, url), (dealPartiesError, dealPartiesResponse, dealPartiesBody) => {
      if (dealPartiesError) {
        // console.log('dealPartiesError :', dealPartiesError);
        reject(err);
      }
      else {
        var resp = JSON.parse(dealPartiesBody);
        var responsibility = [];
        resp.forEach(async (element, index) => {
          let party = {
            dealPartiesId: element._id,
            partyResponsibilityId: element.responsibility,
            partyId: element.partyId,
          };
          // var partyResponsibilityUrl = config.partyResponsibility + '?filter={"_id":{"$in":' + JSON.stringify(responsibility) + '}}&count=-1';
          // getDealResponsibility(req, element.responsibility).then(function (res) {
          //   party.responsibility = res.name;
          //   responsibility.push(party);
          //   if (index == resp.length - 1) {
          //     resolve(responsibility);
          //   }
          // }).catch(err => { })

          let res = await getDealResponsibility(req, element.responsibility);
          party.responsibility = res.name;
          // console.log('party.responsibility  :', party.responsibility);
          await responsibility.push(party);
          if (index == resp.length - 1) {
            await resolve(responsibility);
          }
        });
      }
    })
  });

}
function getDealResponsibility(req, id) {
  var partyResponsibilityUrl = config.partyResponsibility + '?filter={"_id":' + JSON.stringify(id) + '}&count=-1';
  // console.log('partyResponsibilityUrl :', partyResponsibilityUrl);
  return new Promise((resolve, reject) => {
    request(getOptionObj(req, partyResponsibilityUrl),
      (partyResponsibilityError, partyResponsibilityResponse, partyResponsibilityBody) => {
        if (partyResponsibilityError) {
          reject(err);
        }
        else {
          var partyResp = JSON.parse(partyResponsibilityBody);
          // console.log('responsibilityName :', partyResp[0]);
          resolve(partyResp[0]);
        }
      });

  });
}

Array.prototype.unique = function () {
  let arr = [];
  for (let i = 0; i < this.length; i++) {
    if (!arr.includes(this[i].responsibility)) {
      arr.push(this[i].responsibility);
    }
  }
  return arr;
}


function getProcessingUnitsName(units, processingUnits) {
  var result = [];
  if (units == !undefined || units.length > 0) {
    for (var i = 0; i < processingUnits.length; i++) {
      for (var j = 0; j < units.length; j++) {
        if (processingUnits[i]._id === units[j]) {
          result.push(processingUnits[i].name);
        }
      }
    }
  }
  return result;
}

function getBusinessSegment(id, businessSegments) {
  return businessSegments.find(e => id === e._id).name;
}


function getProduct(id, product) {
  return product.find(e => id === e._id).name;
}

function getAccountDetails(req, accObj) {
  // var accountsUrl = config.accounts.concat('?filter={"accountNumber"=' + encodeURIComponent(JSON.stringify(accObj.accountNumber)) + '}&count=-1'); 
  var accountsUrl = config.accounts.concat('/').concat(accObj.accountNumber);
  console.log('accountsUrl :', accountsUrl);
  return new Promise((resolve, reject) => {
    request(utils.getUrl(req, accountsUrl), (err, resp, body) => {
      // console.log('body :', body);
      if (err) {
        reject(err);
      }
      else {
        if (resp.statusCode == 200) {
          var data = JSON.parse(body);
          // TODO: Dont know if there is an array then how to calculate balance.
          accObj.accountStatus = data.status,
            accObj.accountBalance = data.balances.available,
            accObj.accountOpeningDate = data.openedOn,
            accObj.currency = data.currency
          console.log('obj :', accObj);
          resolve(accObj);
        }
        else {
          // accObj.accountStatus=resp.message;
          // resolve(accObj);
          reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
        }
      }
    })
  })
}




function getDeals(req, res) {
  const copy = [];
  // TODO: Need to take user id from token and hit partycontact api to get partyContact name and pass that name to searchquery
  var partyContactName = 'anmol';
  var filter = [{
    "searchBy": "partyContact",
    "searchQuery": partyContactName
  }]
  var dealUrl = config.liveDeals.concat('?');

  var queryParams = req.query;

  if (queryParams.dealFilter) {
    dealUrl = dealUrl.concat('&dealFilter=' + queryParams.dealFilter);
  }
  if (queryParams.filter) {
    var filt = JSON.parse(queryParams.filter);
    filt.forEach(item=>{
      filter.push(item);
    })
    // filter.push(JSON.parse(queryParams.filter));
  }
  if (queryParams.state) {
    dealUrl = dealUrl.concat('&state=' + queryParams.state);
  }
  dealUrl = dealUrl.concat('&filter=').concat(JSON.stringify(filter)).concat('&count=-1');

  console.log('****** dealUrl :', dealUrl);
  request(getOptionObj(req, dealUrl), function (error, response, body) {
    if (!error && response.statusCode == 200) {
      businessSegmentsCall(req).then(businessSegments => {
        getProducts(req).then(product => {
          getProcessingUnits(req).then(processingUnits => {
            async.eachSeries(JSON.parse(body).data, (element, callback) => {
              let deal = {
                dealName: element.name,
                businessSegment: getBusinessSegment(element.businessSegmentId, businessSegments),
                product: getProduct(element.templateId, product),
                dealStartDate: element.duration.start,
                status: element.status,
                dealId: element.refId,
                processingUnits: getProcessingUnitsName(element.processingUnits, JSON.parse(processingUnits)),
                dealEndDate: element.duration.end,
                attributes: element.attributes,
                contacts: element.contacts,
                documents: element.dealDocs
              };
              async.series([
                function (cb) {
                  if (element.parties.length > 0) {
                    getDealParties(req, element.parties).then(function (res) {
                      deal.parties = res;
                      deal.responsibility = deal.parties.unique();
                      if (deal.parties && deal.parties.length !== 0) {
                        deal.parties.forEach((party, ind) => {
                          party.dealId = element._id;
                          if (ind == Number(deal.parties.length - 1)) {
                            cb();
                          }
                        })
                      } else {
                        cb()
                      }
                    }).catch(err => {
                      console.log('err :', err);
                      cb(err)
                    });
                  } else {
                    cb()
                  }
                },
                function (cb) {
                  if (element.accounts.length > 0) {
                    async.eachSeries(element.accounts, async function (innerElement, innerCallback) {
                      let newDeal = cloneDeep(deal);
                      let accObj = {
                        accountNumber: innerElement.accountNumber,
                      }
                      newDeal.accountName = innerElement.name;
                      newDeal.accountNumber = innerElement.accountNumber;
                      await getAccountDetails(req, accObj).then(function (res) {
                        newDeal.accountStatus = res.accountStatus;
                        copy.push(newDeal);
                        innerCallback;
                      }).catch(err => {
                        console.log('err while hitting account api :', err);
                        // res.status(400).json({
                        //   err: err
                        // });
                        copy.push(deal);
                        innerCallback;
                      });
                    },
                      function () {
                        cb();
                      })
                  } else {
                    copy.push(deal);
                    cb();
                  }
                }
              ], function (err, rs) {
                if (err) {
                  res.status(400).json({
                    err: err
                  });
                } else {
                  callback()
                }
              })
            }, function () {
              res.status(200).json(copy);
            })
          }).catch(err => {
            console.log('err :', err);
            res.status(400).json({
              err: err
            })
          })
        }).catch(err => {
          console.log('err :', err);
          res.status(400).json({
            err: err
          })
        })
      }).catch(err => {
        console.log('err :', err);
        res.status(400).json({
          err: err
        })
      })
    } else {
      if (response.statusCode !== 200) {
        var errObj = utils.getErrorObj(response.statusCode, response.statusMessage);
        res.status(errObj.statusCode).json(errObj);
      } else {
        var errObj = utils.getErrorObj(error.statusCode, error.statusMessage);
        res.status(errObj.statusCode).json(errObj);
      }
    }
  })
}


function getOptionObj(req, url) {
  var options = {
    url: url,
    headers: {
      'Authorization': req.headers.authorization
    }
  };
  return options;
}


function getPartyName(req, id) {
  var partyId = config.parties.concat('/').concat(id);
  return new Promise((resolve, reject) => {
    request(getOptionObj(req, partyId),
      function (err, response, body) {
        if (err) {
          // TODO:
          reject(err);
        }
        else {
          if (response.statusCode == 200) {
            resolve(JSON.parse(body).name);
          }
          else {
            reject(utils.getErrorObj(response.statusCode, response.statusMessage));
          }
        }
      });
  });
}

// Want to call multiple api's at a time
function getParties(req, res) {
  var filter = JSON.parse(req.query.filter);
  async.eachSeries(filter, (element, callback) => {
    var count = 0;
    getPartyName(req, element.partyId).then(function (res) {
      count++;
      element.name = res;
      if (count === 2) { callback(); }
    }).catch(err => {
      res.status(err.statusCode).json(err);
    })

    getDealPartyContacts(req, element.partyId, element.dealId).then(function (res) {
      count++;
      element.contacts = res;
      if (count === 2) { callback(); }
    }).catch(err => {
      console.log('err :', err);
      res.status(err.statusCode).json(err);
    })
  }, function () {
    res.json(filter);
  });
}

// function getParties(req, res) {
//   var filter = JSON.parse(req.query.filter);
//   async.eachSeries(filter, (element, callback) => {
//       async.eachSeries([
//           function (cal) {
//               getPartyName(req, element.partyId).then(function (res) {
//                   element.name = res;
//                   cal();
//               }).catch(err => {
//                   console.log('err :', err);
//                   cal();
//               })
//           },
//           function (cal) {
//               getDealPartyContacts(req, element.partyId, element.dealId).then(function (res) {
//                   element.contacts = res;
//                   cal();
//               }).catch(err => {
//                   console.log('err :', err);
//                   cal();
//               })
//           }
//       ], function (err, re) {
//           callback()
//       })
//   }, function () {
//       res.json(filter);
//   });
// }

function getDealPartyContacts(req, partyId, dealId) {
  return new Promise((resolve, reject) => {
    var url = config.dealPartyContacts + '?filter={"partyId":' + JSON.stringify(partyId) + ',"dealId":' + JSON.stringify(dealId) + '}&count=-1';
    request(getOptionObj(req, url), (err, response, body) => {
      if (err) {
        reject(err);
      }
      else {
        if (response.statusCode == 200) {
          var resp = JSON.parse(body);
          var responsibility = [];
          var count = 0;
          if (resp.length > 0) {
            resp.forEach(element => {
              getContact(req, element.partyContactId).then(function (res) {
                res = JSON.parse(res);
                var party = {
                  name: res.name,
                  email: res.email,
                  mobileNumber: res.mobileNumber,
                  designation: res.designation
                };
                responsibility.push(party);
                count++;
                if (count == resp.length) {
                  console.log('responsibility :', responsibility);
                  resolve(responsibility);
                }
              }).catch(err => { console.log('err :', err); })
            });

          }
        }
        else {
          reject(utils.getErrorObj(response.statusCode, response.statusMessage));
        }

      }
    })
  })
}


function getContact(req, partyContactId) {
  return new Promise((resolve, reject) => {
    var mdmPartyContacts = config.mdmPartyContacts.concat('/').concat(partyContactId);
    request(getOptionObj(req, mdmPartyContacts), (partyContactErr, partyContactResp, partyContactBody) => {
      if (partyContactErr) {
        reject(partyContactErr);
      }
      else {
        resolve(partyContactBody);
      }
    })
  });
}


function getContactDetails(req, res) {
  var contacts = req.query.filter;
  var contactUrl = config.contacts.concat('?filter={"_id":{"$in":').concat(contacts).concat('}}&count=-1');
  request(getOptionObj(req, contactUrl), (err, response, body) => {
    if (err) {
      reject(err);
    }
    else {
      if (response.statusCode == 200) {
        var contacts = JSON.parse(body);
        async.each(contacts, function (element, callback) {
          getContactTypes(req, element.roleId).then(function (resp) {
            element.designation = JSON.parse(resp).name;
            delete element['_metadata'];
            delete element['_id'];
            delete element['__v'];
            callback();
          }).catch(err => {
            console.log('err :', err);
          })
        }, function (err) {
          if (err) {
            res.status(err.statusCode).json(err);
          }
          else {
            res.json(contacts);
          }
        })
      }
      else {
        var errObj = utils.getErrorObj(response.statusCode, response.statusMessage);
        res.status(errObj.statusCode).json(errObj);
      }
    }
  })
}

function getContactTypes(req, roleId) {
  return new Promise((resolve, reject) => {
    var contactTypes = config.contactTypes.concat('/').concat(roleId);
    request(getOptionObj(req, contactTypes), (contactErr, contactResp, contactBody) => {
      if (contactErr) {
        reject(contactErr);
      }
      else {
        if (contactResp.statusCode == 200) {
          resolve(contactBody);
        }
        else {
          var errObj = utils.getErrorObj(response.statusCode, response.statusMessage);
          res.status(errObj.statusCode).json(errObj);
        }
      }
    })
  })
}

// todo: Document api is pending have dependency on XCRO product.


function getDoucments(req, res) {
  var filter = req.query.filter;
  var params = JSON.parse(filter);
  var partyId = params.partyId;
  var dealId = params.dealId;

  var result = async.series([
    function (callback) {
      var dealDocumentPromise = getDealDocuments(req, 'mandatory', dealId);
      dealDocumentPromise.then(function (data) {
        callback(null, data);
      }).catch(err=>{
        console.log('err :', err);
        res.status(err.statusCode).json(utils.getErrorObj(err.statusCode, err.message));
      });
    },

    function (callback1) {
      var dealDocumentPromise = getDealDocuments(req, 'optional', dealId);
      dealDocumentPromise.then(function (data) {
        callback1(null, data);
      }).catch(err=>{
        console.log('err :', err);
        res.status(err.statusCode).json(utils.getErrorObj(err.statusCode, err.message));
      });
    },

    function (callback2) {
      var dealDocumentPromise = getDealReqDocuments(req, 'required', dealId);
      dealDocumentPromise.then(function (data) {
        callback2(null, data);
      }).catch(err=>{
        console.log('err :', err);
        res.status(err.statusCode).json(utils.getErrorObj(err.statusCode, err.message));
      });
    },

    function (callback3) {
      var dealPartyContact = getDealPartyDocuments(req, partyId, dealId);
      dealPartyContact.then(function (data) {
        callback3(null, data);
      }).catch(err=>{
        console.log('err :', err);
        res.status(err.statusCode).json(utils.getErrorObj(err.statusCode, err.message));
      });
    }
  ], (err, result) => {
    if (err) {
      res.status(err.statusCode).json(utils.getErrorObj(err.statusCode, err.statusMessage));
    }
    else {
      var data = [];
      console.log('result :', result);
      result.forEach((element, index) => {
        element.forEach((item, index) => {
          data.push(item);
        })
      })
      res.json(data);
    }
  })

  function getDealReqDocuments(req, type, dealId) {
    var url = config.dealDocuments.concat('?filter={"type":' + JSON.stringify(type) + ',"dealId":' + JSON.stringify(dealId) + '}&count=-1');
    console.log('url :', url);
    return new Promise((resolve, reject) => {
      request(utils.getUrl(req, url), (err, resp, body) => {
        if (err) {
          reject(err);
        }
        else {
          var finalRes = [];
          if (resp.statusCode == 200) {
            var reqDocuments = JSON.parse(body);
            async.eachSeries(reqDocuments,async function(doc,callback){
              await getDocumentType(req, doc.documentTypeId).then(function (r) {
                let result = {
                  documentType : r
                }
                finalRes.push(result);
                callback;
              }).catch(err => { console.log('err :', err); callback; })
            },function(){
              resolve(finalRes);
            })
          }
          else {
            resolve('');
          }
        }
      })


    })

  }










  function getDealPartyDocuments(req, partyId, dealId) {
    var url = config.dealPartyDocuments.concat('?filter={"partyId":{"$in":' + JSON.stringify(partyId) + '},"dealId":' + JSON.stringify(dealId) + '}&count=-1');
    console.log('url :', url);
    return new Promise((resolve, reject) => {
      request(utils.getUrl(req, url), (err, resp, body) => {
        if (err) {
          reject(err);
        }
        else {
          var finalRes = [];
          if (resp.statusCode == 200) {
            var partyDocuments = JSON.parse(body);
            if (partyDocuments && partyDocuments.length > 0) {
              async.eachSeries(partyDocuments, (element, callback) => {
                let partyDoc = getDealDocs(req, element.partyDocumentId);
                partyDoc.then(function (items) {
                  console.log('items :', items);
                  async.eachSeries(items, async function (item, innerCallback1) {
                    console.log('item :', item);
                    let result = {
                      hrefId: item.hrefId,
                      fileName: item.fileName,
                      uploadProgress: item.uploadProgress,
                      uploadDate: item.singleDate
                    };
                    await getDocumentType(req, item.documentTypeId).then(function (r) {
                      result.documentType = r;
                      finalRes.push(result);
                      innerCallback1;
                    }).catch(err => { console.log('err :', err); innerCallback1; })
                  }, function () {
                    callback();
                  })
                }).catch(err => {
                  console.log('err :', err);
                })
              }, function () {
                resolve(finalRes);
              });

            }
            else{
              resolve([]);
            }
          }
          else {
            resolve([]);
          }
        }
      })


    })

  }

  function getDealDocs(req, id) {
    return new Promise((resolve, reject) => {
      var partyDocuments = config.partyDocuments.concat('?filter={"_id":' + JSON.stringify(id) + '}&count=-1');
      request(utils.getUrl(req, partyDocuments), (err, resp, body) => {
        if (err) {
          reject(err);
        }
        else {
          if (resp.statusCode == 200) {
            var data = JSON.parse(body);
            resolve(data);
          }
          else {
            reject()
          }
        }


      })
    })
  }


  function getDealDocuments(req, type, dealId) {
    var url = config.dealDocuments.concat('?filter={"type":' + JSON.stringify(type) + ',"dealId":' + JSON.stringify(dealId) + '}&count=-1');
    console.log('url :', url);
    return new Promise((resolve, reject) => {
      request(utils.getUrl(req, url), (err, resp, body) => {
        if (err) {
          reject(err);
        }
        else {
          if (resp.statusCode == 200) {
            var data = [];
            var dealDocs = JSON.parse(body)
            async.eachSeries(dealDocs, (element, callback) => {
              if (element.documents && element.documents.length > 0) {
                async.eachSeries(element.documents, async (document, innerCallback) => {
                  let doc = document;
                  await getDocumentType(req, element.documentTypeId).then(function (r) {
                    doc.documentType = r;
                    data.push(doc);
                    innerCallback;
                  }).catch(err => { console.log('err :', err); innerCallback; })

                }, function () {
                  callback();
                })
              }
              else {
                callback();
              }
            }, function () {
              resolve(data);
            })
          }
          else {
             reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
          }
        }
      })


    })

  }



  function getDocumentType(req, id) {
    var url = config.documentType.concat('/').concat(id);
    return new Promise((resolve, reject) => {
      request(utils.getUrl(req, url), (err, resp, body) => {
        if (err) {
          reject(err);
        }
        else {
          if (resp.statusCode == 200) {
            var data = JSON.parse(body);
            resolve(data.name);
          }
          else {
            reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
          }
        }
      })
    })
  }
}