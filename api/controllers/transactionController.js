'use strict';

var request = require('request');
const async = require("async");
const config = require("../helpers/config");
const utils = require('../helpers/utils');

const express = require('express');
const router = express.Router();

const statusEnum = {
    SCHD: 'Scheduled Transactions/Instructions',
    RJCT: 'Rejected Transactions / Instructions',
    failed: 'Failed  Transactions / Instructions',
    ACSC: 'Settled  Transactions / Instructions in all cases i.e internal / NEFT / RTGS',
    ACSP: 'Settlement In Progress  Transactions / Instructions in case of only NEFT / RTGS',
    PNDG: 'Pending Transactions / Instructions',
    IGNR: 'Ignored Transactions / Instructions'
}


function getLastOrDetailTransactions(req, res) {
    var url;
    var filter = {}
    if (req.query.filter) {
        filter = JSON.parse(req.query.filter);
    }
    if ('last' === filter['type']) {
        delete filter['type'];
        url = config.Transactions.concat('?filter=').concat(JSON.stringify(filter)).concat('&count=5');
    }
    else if ('detail' === filter['type']) {
        delete filter['type'];
        url = config.Transactions.concat('?filter=').concat(JSON.stringify(filter));
        if (req.query.page) {
            url = url.concat('&page=' + req.query.page);
        }
        if (req.query.count) {
            url = url.concat('&count=' + req.query.count);
        }
    }

    console.log('url :', url);
    getDealTransactions(req, url).then(function (resp) {
        var finalResult = []
        if (resp.length > 0) {
            let count = 0;
            resp.forEach(element => {
                getTransactionObj(element, req).then(function (result) {
                    finalResult.push(result);
                    count++;
                    if (count == (resp.length)) {
                        res.json(finalResult)
                    }
                }).catch(err => {
                    console.log('err :', err);
                });
            });
        }
        else {
            res.json({ 'message': 'No data found' });
        }
    }).catch(err => {
        var errObj = utils.getErrorObj(err.statusCode, error.statusMessage);
        res.status(err.statusCode).json(errObj);
    })
}


function getDealTransactions(req, url) {
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, url), (err, response, body) => {
            if (err) {
                reject(utils.getErrorObj(err.statusCode, err.message));
            }
            else {
                if (response.statusCode == 200) {
                    var resp = JSON.parse(body);
                    resolve(resp);
                }
                else {
                    reject(utils.getErrorObj(response.statusCode, response.statusMessage));
                }
            }
        })
    })

}


async function getTransactionObj(element, req) {
    console.log('element :', element);
    let result = {
        dealName: element.dealName,
        paymentInstrument: element.paymentInstrument,
        transactionName: element.instructionName,
        transactionPurpose: element.instructionPurposeName,
        sourceAccount: element.sourceAccountNumber,
        destinationAccount: element.destinationAccountNumber,
        transactionAmount: element.settledAmount,
        beneficiaryName: element.beneficiaryName,
        bicCode: element.ifscCode ? element.ifscCode : "",
        budgetPurpose: element.subInstructionPurpose,
        type: element.transactionType,
        status: statusEnum[element.status],
        executionStartAt: element.executionStartAt,
        transactionId: element._id,
        dealId: element.refId,
        startDate: element.startDate,
        endDate: element.endDate
    }
    if (element.status === "RJCT" || element.status === "failed") {
        result.rejectReason = element.errorList
    }
    // element.budgetId = "BG239"
    if (element.budgetId) {
        var budgetName = await getBudgetName(req, element.budgetId);
        result.budget = budgetName;
    }
    return result;
}


function getBudgetName(req, budgetId) {
    var url = config.budgetGroup.concat('?filter={"$or":[{"_id":' + JSON.stringify(budgetId) + '},{"history.ids":{"$in":[' + JSON.stringify(budgetId) + ']}}]}');;
    console.log('url :', url);
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, url), (err, response, body) => {
            if (err) {
                reject(err);
            }
            else {
                if (response.statusCode == 200) {
                    var resp = JSON.parse(body);
                    resolve(resp[0].name);
                    // return resp[0].name;

                }
                else {
                    reject();
                }
            }
        })
    })
}





function getTransactions(req, res) {
    var partyContactName = 'anmol';
    var filter = [{
        "searchBy": "partyContact",
        "searchQuery": partyContactName
    }]
    var dealUrl = config.liveDeals.concat('?').concat('&filter=').concat(JSON.stringify(filter)).concat('&count=-1');

    getDeal(req, dealUrl).then(function (result) {
        var refIds = [];
        result.forEach(element => {
            refIds.push(element.refId)
        })
        var url = config.Transactions;
        var TransactionFilter = {
            "refId": {
                "$in": refIds
            }
        }
        if (req.query.filter) {
            var filter = JSON.parse(req.query.filter);
            if (filter['status']) {
                var key;
                Object.keys(statusEnum).some(function (k) {
                    if (statusEnum[k] === filter['status']) {
                        key = k;
                        return key;
                    }
                });
                filter['status'] = key;
            }
            filter['refId'] = TransactionFilter.refId;
            if (filter['instructionName']) {
                filter['instructionName'] = '/' + filter.instructionName + '/';
            }
            if (filter['isUpcoming']) {
                delete filter.isUpcoming
                var obj = {
                    "$lt": ((new Date()).getTime() + 1000*60*60*24*15),
                    "$gte": ((new Date()).getTime())
                }
                filter.endDate = obj;
            }
            console.log('filter :', filter);
            url = url.concat('?filter=').concat(JSON.stringify(filter));
        }
        else {
            url = url.concat('?filter=').concat(JSON.stringify(TransactionFilter));
        }
        if (req.query.page) {
            url = url.concat('&page=' + req.query.page);
        }
        if (req.query.count) {
            url = url.concat('&count=' + req.query.count);
        }
        getDealTransactions(req, url).then(function (Transactions) {
            var finalResult = []
            if (Transactions.length > 0) {
                let count = 0;
                Transactions.forEach(element => {
                    getTransactionObj(element, req).then(function (result) {
                        finalResult.push(result);
                        count++;
                        if (count == (Transactions.length)) {
                            res.json(finalResult)
                        }
                    }).catch(err => {
                        console.log('err :', err);
                    });
                });
            }
            else {
                res.json({ 'message': 'No data found' });
            }
        }).catch(err => {
            console.log('err :', err);
        })

    }).catch(err => {
        console.log('err :', err);
        res.status(err.statusCode).json(err);
    })
}


function getDeal(req, dealUrl) {
    return new Promise((resolve, reject) => {
        request(utils.getUrl(req, dealUrl), function (error, response, body) {
            if (error) {
                reject(utils.getErrorObj(err.statusCode, err.message));
            }
            else {
                if (response.statusCode == 200) {
                    var resp = JSON.parse(body);
                    var data = resp['data'];
                    resolve(data);
                }
                else {
                    reject(utils.getErrorObj(response.statusCode, response.statusMessage));
                }

            }

        })
    })
}



function getPurpose(req, res) {
    var finalResponse = [];
    var filter = JSON.parse(req.query.filter);
    var dealUrl = config.liveDeals.concat('?').concat('&filter=').concat(JSON.stringify(filter));
    getDeal(req, dealUrl).then(function (result) {
        result.forEach((element, index) => {
            var purposes = element.purposes;
            purposes.forEach(purpose => {
                finalResponse.push(purpose.name);
            })
            if ((result.length - 1) === index) {
                res.json(finalResponse);
            }
        })
    }).catch(err => {
        console.log('err :', err);
    })
}

// module.exports = router;
module.exports = {
    getTransactions: getTransactions,
    getLastOrDetailTransactions: getLastOrDetailTransactions,
    getPurpose: getPurpose,
    statusEnum: statusEnum
}