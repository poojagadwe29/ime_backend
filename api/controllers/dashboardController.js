'use strict';

var request = require('request');
const async = require("async");
const config = require("../helpers/config");
const utils = require('../helpers/utils');
var dealController = require('../controllers/dealController');
module.exports = {
  getDocsToBeSubmitted: getDocsToBeSubmitted,
  getDealsNearExpiration: getDealsNearExpiration
}

function getDocumentType(req, id) {
  var url = config.documentType.concat('/').concat(id);
  return new Promise((resolve, reject) => {
    request(utils.getUrl(req, url), (err, resp, body) => {
      if (err) {
        reject(utils.getErrorObj(err.statusCode, err.message));
      }
      else {
        if (resp.statusCode == 200) {
          var data = JSON.parse(body);
          resolve(data.name);
        }
        else {
          reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
        }
      }
    })
  })
}


function getDocsToBeSubmitted(req, res) {
  var finalRes = [];
  var partyContactName = 'anmol';
  var filter = [{
    "searchBy": "partyContact",
    "searchQuery": partyContactName
  }]
  var dealUrl = config.liveDeals.concat('?').concat('&filter=').concat(JSON.stringify(filter)).concat('&count=-1');
  utils.getDeal(req, dealUrl).then(function (results) {
    var refIds = [];
    results.forEach(result => refIds.push(result.refId));
    console.log('refIds :', refIds);
    var docFilter = {
      "refId": {
        "$in": refIds
      },
      "scheduleType": "DOC"
    }
    var url = config.documentScheduler.concat('?filter=' + JSON.stringify(docFilter)).concat('&count=3').concat('&page=1')
    request(utils.getUrl(req, url), (err, resp, body) => {
      if (err) {
        reject(utils.getErrorObj(err.statusCode, err.message));
      }
      else {
        if (resp.statusCode == 200) {
          var dealDocs = JSON.parse(body);
          async.eachSeries(dealDocs.data, async (document, innerCallback) => {
            let doc = {
              dealName: document.dealName,
              scheduledOn: document.scheduledOn
            }

            await getDocumentType(req, document.documentTypeId).then(function (r) {
              doc.documentType = r;
              finalRes.push(doc);
              innerCallback;
            }).catch(err => { console.log('err :', err); innerCallback; })

          }, function () {
            res.json(finalRes);
          })
        }
        else {
          res.status(resp.statusCode).json(utils.getErrorObj(resp.statusCode, resp.statusMessage));
        }
      }
    })
  }).catch(err => {
    res.status(err.statusCode).json(err);
    console.log('err :', err);
  })
}


function getDocumentType(req, id) {
  var url = config.documentType.concat('/').concat(id);
  return new Promise((resolve, reject) => {
    request(utils.getUrl(req, url), (err, resp, body) => {
      if (err) {
        reject(utils.getErrorObj(err.statusCode, err.message));
      }
      else {
        if (resp.statusCode == 200) {
          var data = JSON.parse(body);
          resolve(data.name);
        }
        else {
          reject(utils.getErrorObj(resp.statusCode, resp.statusMessage));
        }
      }
    })
  })
}

function getDealsNearExpiration(req, res) {
  var finalRes = [];
  var partyContactName = 'anmol';
  var filter = [{
    "searchBy": "partyContact",
    "searchQuery": partyContactName
  }]

  var todayDate = new Date();
  todayDate.setMinutes('00');
  todayDate.setHours('00');
  todayDate.setSeconds('00');
  todayDate.setMilliseconds('000');

  var fromDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 14 * 15);
  fromDate.setHours(23);
  fromDate.setMinutes(59);
  fromDate.setSeconds(59);
  fromDate.setMilliseconds('999');

  var data = {
    "$and": [
      { "duration._end": { "$gte": new Date(todayDate) } },
      { "duration._end": { "$lte": new Date(fromDate) } }
    ]
  }

  var dealUrl = config.liveDeals.concat('?').concat('&filter=').concat(JSON.stringify(filter))
    .concat('&dealFilter=' + JSON.stringify(data))
    .concat('&count=-1');
  console.log('dealUrl :', dealUrl);
  utils.getDeal(req, dealUrl).then(function (results) {
    results.forEach(element => {
      var obj = {
        name: element.name,
        count: (new Date(element.duration.end).getTime() - new Date().getTime()) / (1000 * 3600 * 24),
      }
      finalRes.push(obj);
    })
    res.json(finalRes);
  }).catch(err => {
    console.log('err :', err);
    res.json(err);
  })
}