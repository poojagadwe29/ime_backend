'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
const async = require("async");
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  var port = process.env.PORT || 30470;
  console.log("PORT STARTED : ", port)
  app.listen(port);
  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});




// const accounts = require('../IME/api/controllers/accountController');
// const Transactions =  require('./api/controllers/TransactionController');

// app.use("/accounts",accounts);
// app.use("/Transactions",Transactions);


